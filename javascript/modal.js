jQuery.fn.modal = function(options){
	if(options === null || options === "open"){
		this.addClass("open");
	}else if(options === "close"){
		this.removeClass("open");
	}else if(options === "toggle"){
		this.toggleClass("open");
	}
}

jQuery(document).on("click","*[rel='modal']", function(){
	var modal = jQuery(this).attr("data-target");
	jQuery(modal).modal("open");
});

jQuery(document).on("click","*[rel='modal-close']", function(){
	var modal = jQuery(this).closest(".modal");
	jQuery(modal).modal("close");
});
