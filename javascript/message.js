jQuery.message = function(options){
	if( typeof options === "object"){
		var message = jQuery("<div />",{class: "message " + options.type, html: options.text});
		jQuery("body").prepend(message);
		setTimeout(function(){
			jQuery("body").find(".message").remove();
		},5000);
	}
};