var gulp         = require('gulp');
var gp_concat    = require('gulp-concat');
var sourcemaps   = require('gulp-sourcemaps');

var paths = {
  scripts: ['javascript/**/*.js']
};

/*
* Compile all JS dependece & Minify
*/
gulp.task('scripts', function() {
  return gulp.src(paths.scripts)
  .pipe(sourcemaps.init())
  .pipe(gp_concat('gws.js'))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('./dist/js'));
});

/*
* Gulp Watch Task
*/
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['scripts']);
});


gulp.task('default', ['watch', 'scripts']);
